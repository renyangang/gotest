package main

import (
	"net/http"

	"gitee.com/renyangang/gotest/config"
	log "gitee.com/renyangang/gotest/fdlog"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var db *sqlx.DB

//连接数据库
func init() {
	var err error
	//Db, err := sqlx.Open("数据库类型", "用户名:密码@tcp(地址:端口)/数据库名?charset=编码")
	db, err = sqlx.Open("mysql", "root:123456@tcp(db:3306)/test?charset=utf8mb4")
	if err != nil {
		panic(err)
	}
}

func indexApi(c *gin.Context) {
	log.Info("demoapp res...")
	var info string
	err := db.Get(&info, "SELECT n from test limit 1")
	if err != nil {
		log.Infof("select error %v", err)
		c.String(http.StatusOK, "i am demoapp: "+err.Error())
		return
	}
	c.String(http.StatusOK, "i am demoapp: "+info)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", indexApi)
	return router
}

func main() {
	log.Info("demoapp init...")
	router := initRouter()
	router.Run(":" + config.Port())
}
