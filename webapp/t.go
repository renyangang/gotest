package main

import (
	"net/http"

	"gitee.com/renyangang/gotest/config"
	log "gitee.com/renyangang/gotest/fdlog"
	"github.com/gin-gonic/gin"
)

func indexApi(c *gin.Context) {
	log.Info("webapp res...")
	c.String(http.StatusOK, "i am webapp")
}

func initRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", indexApi)
	return router
}

func main() {
	log.Info("webapp init...")
	router := initRouter()
	router.Run(":" + config.Port())
}
