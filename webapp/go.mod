module webapp

go 1.15

require (
	gitee.com/renyangang/gotest v1.4.0
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/viper v1.7.1 // indirect
)
