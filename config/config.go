package config

import (
	"fmt"

	"github.com/spf13/viper"
)

const path = "./config"
const file = "config.yml"

// Fconfig is global config obj
var fc *Config

func init() {
	fc = new(Config)
	fc.init()
}

/*
Config for fudao
*/
type Config struct {
	v          *viper.Viper
	configfile string
}

func (c *Config) init() {
	c.v = viper.New()
	c.v.AddConfigPath(path)
	c.v.SetConfigName(file)
	c.v.SetConfigType("yaml")
}

// Show is show
func Show() {
	fmt.Println("ok")
}

// Port  for web port
func Port() string {
	return "8080"
}
